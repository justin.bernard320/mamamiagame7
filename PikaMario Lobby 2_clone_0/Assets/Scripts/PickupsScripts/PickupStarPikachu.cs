using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PickupStarPikachu : NetworkBehaviour
{
    [SyncVar]
    public bool bStarMode;
    [SyncVar]
    public float starModeDuration = 8.2f;
    public AudioSource starModeSFX;
    PlayerController_Platform playerController;
    public float starMoveSpeed = 32.0f;
    public Material yellowSkin;
    public Material starModeSkin;
    public Renderer rend = null;

   // public AudioClip[] deathClips; example


    // Start is called before the first frame update
    void Start()
    {
        playerController = GetComponent<PlayerController_Platform>();
        //rend.sharedMaterial = yellowSkin;
    }

    // Update is called once per frame
    void Update()
    {
        //if (!hasAuthority) { return; }

        if (bStarMode)
        {
            playerController.moveSpeed = starMoveSpeed;
            rend.sharedMaterial = starModeSkin;
        }
        if (!bStarMode)
        {
            playerController.moveSpeed = playerController.moveSpeedMax;
            rend.sharedMaterial = yellowSkin;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (GetComponent<PlayerSwitch>().isMario) { return; }

        if (col.gameObject.tag == "Star")
        {
            if (bStarMode) { return; }

            //if (!hasAuthority) { return; }

            rend.sharedMaterial = starModeSkin;
            CmdPlayStarSFX();
            starModeSFX.Play();
            StartCoroutine(StarMode());
            //starModeSFX.Play();

            StartCoroutine(DestroyStar(col.gameObject));
           // Destroy(col.gameObject);
           //1 NetworkServer.Destroy(col.gameObject);
        }
    }

    IEnumerator DestroyStar(GameObject star)
    {
        yield return new WaitForSeconds(0.0f);

        Destroy(star.gameObject);
        NetworkServer.Destroy(star.gameObject);
    }

    //void OnCollisionEnter(Collision col)
    //{
    //    if (col.gameObject.tag == "Star")
    //    {
    //        if (bStarMode) { return; }

    //        StartCoroutine(StarMode());
    //        CmdPlayStarSFX();

    //        Destroy(col.gameObject);
    //    }
    //}

    IEnumerator StarMode()
    {
        bStarMode = true;

        yield return new WaitForSeconds(starModeDuration);

        bStarMode = false;
        rend.sharedMaterial = yellowSkin;
        starModeSFX.Stop();
        CmdStopStarSFX();

    }

    [Command]
    public void CmdPlayStarSFX()
    {
        //if (!hasAuthority) { return; }
        starModeSFX.Play();
        RpcPlayStarSFX();
        rend.sharedMaterial = starModeSkin;

    }
    [ClientRpc]
    void RpcPlayStarSFX()
    {
        starModeSFX.Play();
        rend.sharedMaterial = starModeSkin;
    }

    [Command]
    public void CmdStopStarSFX()
    {
        //if (!hasAuthority) { return; }
        RpcStopStarSFX();
        rend.sharedMaterial = yellowSkin;
    }
    [ClientRpc]
    void RpcStopStarSFX()
    {
        starModeSFX.Stop();
        rend.sharedMaterial = yellowSkin;

    }


    //[Command]
    //public void CmdNormalSkin()
    //{
    //    //if (!hasAuthority) { return; }
    //    RpcNormalSkin();
    //}
    //[ClientRpc]
    //void RpcNormalSkin()
    //{
    //    rend.sharedMaterial = yellowSkin;
    //}


    //[Command]
    //public void CmdStarSkin()
    //{
    //    //if (!hasAuthority) { return; }
    //    RpcStarSkin();
    //}
    //[ClientRpc]
    //void RpcStarSkin()
    //{
    //    rend.sharedMaterial = starModeSkin;
    //}
}
