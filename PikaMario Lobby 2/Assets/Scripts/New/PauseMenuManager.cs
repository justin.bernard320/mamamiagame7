using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Mirror;

public class PauseMenuManager : MonoBehaviour
{
    bool bShowupMenu = false;
    public GameObject pauseMenu = null;


    void Start()
    {

    }

    //IEnumerator UpdateClient()
    void Update()
    {
        //while (true)
        {
            PauseMenuFunction();

            //yield return null;
        }
    }

    void PauseMenuFunction()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            bShowupMenu = !bShowupMenu;
        }

        if (bShowupMenu)
        {
            pauseMenu.SetActive(true);
        }
        if (!bShowupMenu)
        {
            pauseMenu.SetActive(false);
        }

    }
}
